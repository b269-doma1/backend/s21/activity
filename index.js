

let users = ["Dwayne Johnson", "Steve Austin", "Kurt Angle", "Dave Bautista"];

console.log("Original Array:")
console.log(users);

// To add an item to the array
function addItem(item){
	users[users.length] = item;
}

addItem("John Cena");
console.log(users);


// To get an item by its index
function getItemByIndex(index){
	return users[index];
}

console.log(getItemByIndex(2));


// To delete the last item in the array and return it
function deletedLastItem(){
	// Gets the last item in the array an stores it in a variable
	let deleted_item = users[users.length - 1]

	// Decrements the length of the array to make sure that the last item is removed
	users.length--

	return deleted_item;
}

console.log(deletedLastItem());
console.log(users);


// To update an item by the index
function updateItemByIndex(updated_name, index){
	users[index] = updated_name;
}

updateItemByIndex("MJF", 3);
console.log(users);


// To delete all items in the array
function deleteAllItems(){
	users = [];
}

deleteAllItems();
console.log(users);


// To check if the users array is empty
function isUsersArrayEmpty(){
	if(users.length > 0){
		return false
	} else {
		return true
	}

	// Ternary equivalent
	// (users.length > 0) ? return false : return true;
}

console.log(isUsersArrayEmpty())


users = ["Dwayne Johnson", "Steve Austin", "Kurt Angle", "Dave Bautista"];

// To get the index of a specific item in the array
function getIndexByItem(item){
	// Loop through the whole users array
	for(let index = 0; index < users.length; index++){
		// Checks if the item passed as an argument matches an existing item in the array
		if(item === users[index]) {
			return index;
		}
	}
}

console.log(getIndexByItem("Kurt Angle"));




